//
//  StoreListViewControllerViewController.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift

class StoreListViewControllerViewController: UIViewController {

    var disposeBag = DisposeBag()
    
    let viewModel = StoreListViewModel()
    
    let cartButton = UIButton()
    let productsTableView = UITableView()
    
//    let results = try! Realm().objects(DemoObject.self).sorted(byKeyPath: "date")
//    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupCartButton(cartButton)
        setupTableView(productsTableView)
        
        updateViewConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTableView(_ tableView: UITableView) {

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        let items = Observable.just(
            viewModel.getProducts()
        )
        
        items
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)) { (row, element, cell) in
                cell.textLabel?.text = "\(element.product!.name) \(element.amount)"
            }
            .disposed(by: disposeBag)
        
        tableView.rx
            .modelSelected(StoreProduct.self)
            .subscribe(onNext:  { element in
                
                self.viewModel.updateStoreProduct(storeProduct: element)
                
                let cell = tableView.cellForRow(at: tableView.indexPathForSelectedRow!)
                cell?.textLabel?.text = "\(element.product!.name) \(element.amount)"
                
                tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
            })
            .disposed(by: disposeBag)
        
        view.addSubview(tableView)
    }
    
    func setupCartButton(_ button: UIButton) {
        
        button.setTitle("Cart", for: .normal)
        button.setTitleColor(.black, for: .normal)
        
        button.rx.tap.subscribe(onNext: {
            // TODO: move control to viewModel, but callback for presenting here
            self.openUserView()
        })
        .addDisposableTo(disposeBag)
        
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.setRightBarButton(barButton, animated: false)
        
    }
    
    func openUserView() {
        
        let nav = UINavigationController()
        let modalController = UserListViewController()
        nav.viewControllers = [modalController]
        
        modalController.modalTransitionStyle = .crossDissolve
        modalController.modalPresentationStyle = .fullScreen
        
        present(nav, animated: true) {
            
        }
    }

}
