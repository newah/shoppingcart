//
//  StoreListViewControllerViewController+Layouts.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import UIKit

import SnapKit

extension StoreListViewControllerViewController {
    
    override func updateViewConstraints() {
        
        if (!didSetupConstraints) {
            
            cartButton.snp.makeConstraints({ make in
                make.height.equalTo(50)
                make.width.equalTo(50)
            })
            
            productsTableView.snp.makeConstraints({ make in
                make.edges.equalToSuperview()
            })
            
            didSetupConstraints = true
        }
        
        super.updateViewConstraints()
        
    }
}
