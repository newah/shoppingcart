//
//  LayoutsExtensions.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import UIKit

private var didSetupConstraintsKey: UInt8 = 0 // We still need this boilerplate

extension NSObject {
    var didSetupConstraints: Bool {
        get {
            return associatedObject(base: self, key: &didSetupConstraintsKey) { () -> Bool in
                return false
            }
        }
        set {
            associateObject(base: self, key: &didSetupConstraintsKey, value: newValue)
        }
    }
    
    func associatedObject<ValueType>(
        base: AnyObject,
        key: UnsafePointer<UInt8>,
        initialiser: () -> ValueType)
        -> ValueType {
            if let associated = objc_getAssociatedObject(base, key)
                as? ValueType { return associated }
            let associated = initialiser()
            objc_setAssociatedObject(base, key, associated,
                                     .OBJC_ASSOCIATION_RETAIN)
            return associated
    }
    func associateObject<ValueType>(
        base: AnyObject,
        key: UnsafePointer<UInt8>,
        value: ValueType) {
        objc_setAssociatedObject(base, key, value,
                                 .OBJC_ASSOCIATION_RETAIN)
    }
    
}
