//
//  UserListViewController.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift

class UserListViewController: UIViewController {

    var disposeBag = DisposeBag()
    
    let viewModel = StoreListViewModel()
    
    let cartButton = UIButton()
    let closeButton = UIButton()
    let productsTableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView(productsTableView)
        setupCartButton(cartButton)
        setupCloseButton(closeButton)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTableView(_ table: UITableView) {
        view.addSubview(table)
    }
    
    func setupCloseButton(_ button: UIButton) {
        button.setTitle("Close", for: .normal)
        button.setTitleColor(.black, for: .normal)
        
        button.rx.tap.subscribe(onNext: {
            print("close pressed")
            self.dismiss(animated: true, completion: {
            })
        })
            .addDisposableTo(disposeBag)
        
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.setLeftBarButton(barButton, animated: false)
    }
    
    func setupCartButton(_ button: UIButton) {
        
        button.setTitle("Cart", for: .normal)
        button.setTitleColor(.black, for: .normal)
        
        button.rx.tap.subscribe(onNext: {
            print("cart pressed?")
        })
            .addDisposableTo(disposeBag)
        
        let barButton = UIBarButtonItem(customView: button)
        navigationItem.setRightBarButton(barButton, animated: false)
        
    }

}
