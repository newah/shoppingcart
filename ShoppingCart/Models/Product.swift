//
//  Product.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class Product: Object {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var price = 0.0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
