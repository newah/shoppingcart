//
//  UserProduct.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class UserProduct: Object {
    dynamic var id = 0

    override static func primaryKey() -> String? {
        return "id"
    }
    
}
