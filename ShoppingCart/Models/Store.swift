//
//  Store.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class Store: Object {
    dynamic var id = 0
    dynamic var name = ""
    var products = List<StoreProduct>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
