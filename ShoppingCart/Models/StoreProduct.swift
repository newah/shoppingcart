//
//  StoreProduct.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class StoreProduct: Object {
    dynamic var id = 0
    dynamic var amount = 0
    dynamic var product : Product?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
