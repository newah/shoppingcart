//
//  User.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class User: Object {
    dynamic var id = 0
    dynamic var name = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
