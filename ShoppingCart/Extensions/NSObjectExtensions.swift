//
//  NSObjectExtensions.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import UIKit

extension NSObject {
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
}
