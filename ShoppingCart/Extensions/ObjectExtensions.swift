//
//  ObjectExtensions.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

extension Object {
    
    //Incrementa ID
    public func IncrementalID<T: Object>(_ type: T.Type) -> Int{
        let realm = try! Realm()
        
        let RetNext: NSArray = Array(realm.objects(type).sorted(byKeyPath: "id")) as NSArray
        let last = RetNext.lastObject
        if RetNext.count > 0 {
            let valor = (last as AnyObject).value(forKey: "id") as? Int
            return valor! + 1
        } else {
            return 1
        }
    }
}
