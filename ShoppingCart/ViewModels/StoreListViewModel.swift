//
//  StoreListViewModel.swift
//  ShoppingCart
//
//  Created by Gintaras Jakimavicius on 19/04/2017.
//  Copyright © 2017 Gintaras Jakimavicius. All rights reserved.
//

import RealmSwift

class StoreListViewModel: NSObject {

    let realm = try! Realm()
    
    var store: Store?
    
    override init() {
        super.init()
        
        store = getStore()
    }
    
    func getProducts() -> List<StoreProduct> {
        return store!.products
    }
    
    private func getStore() -> Store {
        
        var newStore = try! Realm().object(ofType: Store.self, forPrimaryKey: 1)
            
        if newStore == nil {
            newStore = generateStore()
        }
    
        return newStore!
    }
    
    private func generateStore() -> Store {
        let store = Store()
        //increment auto id
        store.id = store.IncrementalID(Store.self)
        store.name = "Some store"
        
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 10))
        store.products.append(generateStoreProduct(name: "Balloon", price: 1.10, amount: 30))
        store.products.append(generateStoreProduct(name: "Flee", price: 40.0, amount: 1))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        store.products.append(generateStoreProduct(name: "Lamp", price: 35.99, amount: 30))
        
//        let storeProducts = [StoreProduct]()
//        
//        if (storeProducts.count == 0) {
//            generateProducts()
//        }
        
        try! realm.write {
            realm.add(store)
        }
        
        return store
    }
    
    func updateStoreProduct (storeProduct: StoreProduct) {
        try! realm.write({
            storeProduct.amount -= 1
            realm.add(storeProduct, update: true)
        })
    }
    
    private func generateStoreProduct(name: String, price: Double, amount: Int) -> StoreProduct {
        let product = Product()
        //increment auto id
        product.id = product.IncrementalID(Product.self)
        product.name = name
        product.price = price
        try! realm.write({ () -> Void in
            realm.add(product)
        })
        
        let storeProduct = StoreProduct()
        storeProduct.product = product
        storeProduct.amount = amount
        storeProduct.id = storeProduct.IncrementalID(StoreProduct.self)
        try! realm.write({ () -> Void in
            realm.add(storeProduct)
        })
        
        return storeProduct
    }
    
}
